package model;

public class Produs {

	private int id;
	private String nume;
	private String producator;
	private int anFabricatie;
	private int anExpirare;
	private float pret;
	private int cantitate;
	
	/**
	 * Constructor gol pentru reflection
	 */
	public Produs() {
		
	}
	
	/**
	 * Creeaza un produs nou
	 * @param id id-ul produsului
	 * @param nume numele
	 * @param producator numele producatorului
	 * @param anFabricatie anul fabricarii
	 * @param anExpirare anul expirarii
	 * @param pret pretul produsului
	 * @param cant cantitatea curenta
	 */
	public Produs(int id, String nume, String producator, int anFabricatie, int anExpirare, float pret, int cant) {
		this.id = id;
		this.nume = nume;
		this.producator = producator;
		this.anFabricatie = anFabricatie;
		this.anExpirare = anExpirare;
		this.pret = pret;
		this.cantitate = cant;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getProducator() {
		return producator;
	}
	public void setProducator(String producator) {
		this.producator = producator;
	}
	public int getAnFabricatie() {
		return anFabricatie;
	}
	public void setAnFabricatie(int anFabricatie) {
		this.anFabricatie = anFabricatie;
	}
	public int getAnExpirare() {
		return anExpirare;
	}
	public void setAnExpirare(int anExpirare) {
		this.anExpirare = anExpirare;
	}
	public float getPret() {
		return pret;
	}
	public void setPret(float pret) {
		this.pret = pret;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	
	public String toString() {
		return this.id + ": " + this.nume;
	}
	
}
