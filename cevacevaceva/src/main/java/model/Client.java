package model;

public class Client {

	private int id;
	private String nume;
	private String prenume;
	private String adresa;
	private String telefon;
	private int varsta;
	
	/**
	 * Constructor gol necesar pentru reflection
	 */
	public Client() {	
	}
	
	/**
	 * Creeaza un client nou
	 * @param id id-ul clientului
	 * @param nume numele
	 * @param prenume prenumele
	 * @param adresa adresa
	 * @param telefon numarul de telefon
	 * @param varsta varsta 
	 */
	public Client(int id, String nume, String prenume, String adresa, String telefon, int varsta) {
		this.id = id;
		this.nume = nume;
		this.prenume = prenume;
		this.adresa = adresa;
		this.telefon = telefon;
		this.varsta = varsta;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public int getVarsta() {
		return varsta;
	}
	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}
	
	public String toString() {
		return this.id + ": " + this.prenume + " " + this.nume;
	}
}
