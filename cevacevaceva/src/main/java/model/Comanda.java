package model;

public class Comanda {

	private int id;
	private int client;
	private int produs;
	private int cantitate;
	private float pret;
	private String data;
	
	/**
	 * Constructor gol pentru reflection
	 */
	public Comanda() {
		
	}
	
	/**
	 * Creeaza o comanda noua
	 * @param i id-ul comenzii
	 * @param client id-ul clientului
	 * @param produs id-ul produsului
	 * @param cantitate cantitatea cumparata
	 * @param pret pretul total
	 * @param data data tranzactiei
	 */
	public Comanda(int i, int client, int produs, int cantitate, float pret, String data) {
		this.id = i;
		this.client = client;
		this.produs = produs;
		this.cantitate = cantitate;
		this.pret = pret;
		this.data = data;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getClient() {
		return client;
	}
	public void setClient(int client) {
		this.client = client;
	}
	public int getProdus() {
		return produs;
	}
	public void setProdus(int produs) {
		this.produs = produs;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public float getPret() {
		return pret;
	}
	public void setPret(float pret) {
		this.pret = pret;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
