package businessLogicLayer;

import javax.swing.JOptionPane;

import dataAccessLayer.ClientDAO;
import model.Client;

public class ValidareClient {

	/**
	 * numele validat
	 */
	private String nume;
	/**
	 * prenumele validat
	 */
	private String prenume;
	/**
	 * adresa validata
	 */
	private String adresa;
	/**
	 * numarul de telefon validat
	 */
	private String telefon;
	/**
	 * varsta validata
	 */
	private int varsta;

	/**
	 * statusul validarii curente
	 */
	private int validat;
	/**
	 * id-ul la care s-a ramas cu numerotarea
	 */
	public static int idCurent = 0;
	
	/**
	 * Constructorul seteaza idCurent la valoarea maxima actuala
	 * din baza de date
	 */
	public ValidareClient() {
		ClientDAO x = new ClientDAO();
		idCurent = x.idMax(); 
	}

	/**
	 * Verifica daca exista un anumit ID in tabelul Client
	 * @param id
	 * id-ul cautat in tabel
	 * @return
	 * returneaza 1 daca exista, altfel 0
	 */
	public int existaId(int id) {
		ClientDAO x = new ClientDAO();
		Client client = x.findById(id); 
		if (id > 0 && client != null) {
			return 1;
		} else {
			JOptionPane.showMessageDialog(null, "ID doesn't exist!", "Fail!", JOptionPane.ERROR_MESSAGE);
			return 0;
		}
	}

	/**
	 * Valideaza datele introduse de utilizator in aplicatie
	 * @param nume
	 * numele introdus de utilizator
	 * @param prenume
	 * prenumele introdus de utilizator
	 * @param adresa
	 * adresa introdusa
	 * @param telefon
	 * numarul de telefon introdus
	 * @param varsta
	 * varsta introdusa
	 * @return
	 * returneaza 1 daca datele sunt valide
	 * -1 daca numele e invalid
	 * -2 daca prenumele e invalid
	 * -3 daca adresa e invalida
	 * -4 daca nr de telefon este invalid
	 * -5 daca varsta este invalida
	 */
	public int validareClient(String nume, String prenume, String adresa, String telefon, String varsta) {
		int ok;
		this.validat = 0;
		ok = validareNume(nume);
		if (ok == -1)
			return -1;
		ok = validarePrenume(prenume);
		if (ok == -1)
			return -2;
		ok = validareAdresa(adresa);
		if (ok == -1)
			return -3;
		ok = validareTelefon(telefon);
		if (ok == -1)
			return -4;
		ok = validareVarsta(varsta);
		if (ok == -1)
			return -5;

		this.validat = 1;
		return 1;
	}

	/**
	 * Creeaza si returneaza un client valid pentru datele primite
	 * @param idBun
	 * daca e <=0, atunci reprezinta un client nou pentru tabel
	 * daca e pozitiv inseamna ca facem update la un client mai vechi
	 * @return
	 * un obiect Client validat sau null in cazul in care nu avem date corecte
	 */
	public Client getClientValidat(int idBun) {
		if (this.validat == 1) {
			if (idBun <= 0) {
				idBun = (++idCurent);
			}
			Client client = new Client(idBun, this.nume, this.prenume, this.adresa, this.telefon, this.varsta);
			return client;
		} else {
			return null;
		}
	}

	/**
	 * Verifica daca numele introdus este corect
	 * @param nume
	 * numele introdus de utilizator
	 * @return
	 * 1 daca e valid, -1 altfel
	 */
	private int validareNume(String nume) {
		String denumire = this.validareDenumire(nume);
		if (denumire == null) {
			return -1;
		} else {
			this.nume = denumire;
			return 1;
		}
	}

	/**
	 * Spune daca un prenume introdus este valid
	 * @param prenume
	 * prenumele introdus
	 * @return
	 * 1 daca e valid, -1 altfel
	 */
	private int validarePrenume(String prenume) {
		String denumire = this.validareDenumire(prenume);
		if (denumire == null) {
			return -1;
		} else {
			this.prenume = denumire;
			return 1;
		}
	}

	/**
	 * Verifica daca o denumire e valida
	 * @param nume
	 * denumirea introdusa
	 * @return
	 * returneaza denumirea validata sau null daca nu s-a putut valida nicicum
	 */
	private String validareDenumire(String nume) {
		nume = nume.replaceAll("^\\s+", ""); // stergem spatiile din fata
		nume = nume.replaceAll("\\s+$", ""); // stergem spatiile din spate
		nume = nume.replaceAll("\\s+", " "); // stergem spatiile interioare care sunt in plus
		if (nume.length() < 1)
			return null; // verificam daca exista ceva ramas

		for (int i = 0; i < nume.length(); i++) {
			if (!((nume.charAt(i) >= 'a' && nume.charAt(i) <= 'z') || (nume.charAt(i) >= 'A' && nume.charAt(i) <= 'Z')
					|| nume.charAt(i) == ' ')) {
				return null;
			}
		}

		nume = nume.toLowerCase();
		return nume;
	}

	/**
	 * Valideaza o adresa introdusa de utilizator
	 * @param adresa
	 * adresa introdusa
	 * @return
	 * 1 daca e valida, 0 altfel
	 */
	private int validareAdresa(String adresa) {
		adresa = adresa.replaceAll("^\\s+", ""); // stergem spatiile din fata
		adresa = adresa.replaceAll("\\s+$", ""); // stergem spatiile din spate
		adresa = adresa.replaceAll("\\s+", " "); // stergem spatiile interioare care sunt in plus
		adresa = adresa.replaceAll(",+", ",");
		adresa = adresa.replaceAll("\\.+", ".");
		adresa = adresa.replaceAll("\\-+", "-");

		if (adresa.length() < 1)
			return -1; // verificam daca exista ceva ramas

		for (int i = 0; i < adresa.length(); i++) {
			if (!((adresa.charAt(i) >= 'a' && adresa.charAt(i) <= 'z')
					|| (adresa.charAt(i) >= 'A' && adresa.charAt(i) <= 'Z')
					|| (adresa.charAt(i) >= '0' && adresa.charAt(i) <= '9') || adresa.charAt(i) == ' '
					|| adresa.charAt(i) == ',' || adresa.charAt(i) == '.' || adresa.charAt(i) == '-')) {
				return -1;
			}
		}

		adresa = adresa.toLowerCase();
		this.adresa = adresa;
		return 1;
	}

	/**
	 * Valideaza un numar de telefon romanesc (cu 07 in fata)
	 * @param tel
	 * numarul de telefon introdus
	 * @return
	 * 1 daca e valid, -1 altfel
	 */
	private int validareTelefon(String tel) {
		tel = tel.replaceAll("^\\s+", ""); // stergem spatiile din fata
		tel = tel.replaceAll("\\s+$", ""); // stergem spatiile din spate

		if (tel.length() != 10) 
			return -1; 
		if (!tel.substring(0, 2).equals("07"))
			return -1; 

		for (int i = 2; i < tel.length(); i++) {
			if (tel.charAt(i) < '0' || tel.charAt(i) > '9')
				return -1;
		}

		this.telefon = tel;
		return 1;
	}

	/**
	 * Valideazao varsta introdusa
	 * @param varsta
	 * varsta introdusa
	 * @return
	 * returneaza 1 daca numarul e valid si peste 18 ani, -1 altfel
	 */
	private int validareVarsta(String varsta) {
		varsta = varsta.replaceAll("^\\s+", ""); // stergem spatiile din fata
		varsta = varsta.replaceAll("\\s+$", ""); // stergem spatiile din spate
		try {
			this.varsta = Integer.parseInt(varsta);
			
			if (this.varsta < 18) {
				return -1;
			} else {
				return 1;
			}
		} catch (Exception e) {
			return -1;
		}

	}

}
