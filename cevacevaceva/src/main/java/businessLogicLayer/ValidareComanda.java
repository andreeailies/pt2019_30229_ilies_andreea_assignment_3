package businessLogicLayer;

import dataAccessLayer.ComandaDAO;
import dataAccessLayer.ProdusDAO;
import model.Comanda;
import model.Produs;

public class ValidareComanda {

	/**
	 * id-ul clientului ce a plasat comanda
	 */
	private int client;
	/**
	 * produsul cumparat
	 */
	private int produs;
	/**
	 * cantitatea cumparata
	 */
	private int cantitate;
	
	/**
	 * validitatea comenzii
	 */
	private int valid = 0;
	/** 
	 * id-ul maxim curent din tabel
	 */
	public static int id = 0;
	
	/**
	 * Constructor pentru verificarea comenzii
	 * @param client
	 * id-ul clientului ce a dat comanda
	 * @param produs
	 * id-ul produsului comandat
	 */
	public ValidareComanda(int client, int produs) {
		this.client = client;
		this.produs = produs;
		ComandaDAO x = new ComandaDAO();
		id = x.idMax();
	}
	
	/**
	 * Verifica validitatea comenzii
	 * @param cantitate
	 * cantitatea introdusa
	 * @return
	 * returneaza -1 daca cantitatea e invalida
	 * -2 daca nu avem suficiente produse in stoc
	 * altfel returneaza cantitatea ramasa din produs in stoc dupa
	 * ce se vand cele comandate acum
	 */
	public int validareComanda(String cantitate) {
		int ok = 0;
		valid = 0;
		ok = this.validareCantitate(cantitate);
		if (ok == -1) return -1;
		ok = this.validareSuficienta();
		if (ok == -1) return -2;
		valid = 1;
		return ok;
	}
	
	/**
	 * Verifica daca cantitatea introdusa e corecta
	 * @param cant
	 * cantitatea introdusa
	 * @return
	 * 1 daca e valida, -1 altfel
	 */
	private int validareCantitate(String cant) {
		cant = cant.replaceAll("^\\s+", ""); // stergem spatiile din fata
		cant = cant.replaceAll("\\s+$", ""); // stergem spatiile din spate
		try {
			this.cantitate = Integer.parseInt(cant);
			
			if (this.cantitate <= 0) {
				return -1;
			} else {
				return 1;
			}
		} catch (Exception e) {
			return -1;
		}
	}
	
	/**
	 * Verifica daca avem suficiente produse
	 * @return
	 * returneaza -1 daca nu avem suficiente produse
	 * altfel returneaza cat a ramas in stoc
	 */
	private int validareSuficienta() {
		ProdusDAO x = new ProdusDAO();
		Produs curent = x.findById(produs);
		int stoc = curent.getCantitate(); // aici extragem stocul
		if (stoc < cantitate) {
			return -1;
		} else 
		if (stoc == cantitate) {
			return 0;
		}
		return stoc - cantitate;
	}
	
	/**
	 * Creeaza obiectul Comanda cu datele valide
	 * @return
	 * un obiect Comanda daca datele sunt valide, null altfel
	 */
	public Comanda getComandaValida() {
		if (valid == 0) return null;
		ProdusDAO x = new ProdusDAO();
		Produs curent = x.findById(produs);
		Comanda comanda = new Comanda((++id),client, produs, cantitate, curent.getPret() * cantitate, "21-07-2024");
		return comanda;
	}

}
