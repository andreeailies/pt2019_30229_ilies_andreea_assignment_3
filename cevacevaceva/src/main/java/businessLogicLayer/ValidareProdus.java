package businessLogicLayer;

import javax.swing.JOptionPane;

import dataAccessLayer.ProdusDAO;
import model.Produs;

public class ValidareProdus {

	private String nume;
	private String producator;
	private int anFabricatie;
	private int anExpirare;
	private float pret;
	private int cantitate;

	private int validat;
	private static int idCurent = 0;

	public ValidareProdus() {
		ProdusDAO x = new ProdusDAO();
		idCurent = x.idMax();
	}

	/**
	 * Verifica daca exista in tabelul Produs un client cu acest id
	 * @param id
	 * @return 1 daca exista, 0 daca nu
	 */
	public int existaId(int id) {
		ProdusDAO x = new ProdusDAO();
		Produs produs = x.findById(id);
		if (id > 0 && produs != null) {
			return 1;
		} else {
			JOptionPane.showMessageDialog(null, "ID doesn't exist!", "Fail!", JOptionPane.ERROR_MESSAGE);
			return 0;
		}
	}

	/**
	 * Valideaza datele produsului introdus
	 * @param nume numele introdus
	 * @param producator numele producatorului introdus
	 * @param anF anul in care a fost fabricat
	 * @param anE anul in care va expira
	 * @param pret pretul produsului
	 * @param cant cantitatea disponibila
	 * @return 
	 * -1 nume invalid
	 * -2 producator invalid
	 * -3 an fabricatie invalid
	 * -4 an expirare invalid
	 * -5 pret invalid
	 * -6 cantitate invalida
	 */
	public int validareProdus(String nume, String producator, String anF, String anE, String pret, String cant) {
		int ok;
		this.validat = 0;
		ok = validareNume(nume);
		if (ok == -1)
			return -1;
		ok = validareProducator(producator);
		if (ok == -1)
			return -2;
		ok = validareAnF(anF);
		if (ok == -1)
			return -3;
		ok = validareAnE(anE);
		if (ok == -1)
			return -4;
		ok = validarePret(pret);
		if (ok == -1)
			return -5;
		ok = validareCantitate(cant);
		if (ok == -1)
			return -6;

		this.validat = 1;
		return 1;
	}

	/**
	 * Creeaza produsul validat
	 * @param idBun daca e <=0 inseamna ca avem nevoie de un produs nou, mai mare inseamna update
	 * @return un obiect Produs daca e valid, null altfel
	 */
	public Produs getProdusValidat(int idBun) {
		if (this.validat == 1) {
			if (idBun <= 0) {
				idBun = (++idCurent);
			}
			Produs produs = new Produs(idBun, this.nume, this.producator, this.anFabricatie, this.anExpirare, this.pret,
					this.cantitate);
			return produs;
		} else {
			return null;
		}
	}

	/**
	 * Valideaza cantitatea introdusa
	 * 
	 * @param cant cantitatea introdusa
	 * @return 1 valid, -1 invalid
	 */
	private int validareCantitate(String cant) {
		cant = cant.replaceAll("^\\s+", ""); // stergem spatiile din fata
		cant = cant.replaceAll("\\s+$", ""); // stergem spatiile din spate
		try {
			this.cantitate = Integer.parseInt(cant);

			if (this.cantitate < 1) {
				return -1;
			} else {
				return 1;
			}
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Valideaza pretul introdus
	 * 
	 * @param pret pretul introdus
	 * @return 1 valid, -1 invalid
	 */
	private int validarePret(String pret) {
		pret = pret.replaceAll("^\\s+", ""); // stergem spatiile din fata
		pret = pret.replaceAll("\\s+$", ""); // stergem spatiile din spate
		try {
			this.pret = Float.parseFloat(pret);
			if (this.pret <= 0) {
				return -1;
			} else {
				return 1;
			}
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Valideaza anul in care expira
	 * 
	 * @param anE anul introdus
	 * @return 1 valid, -1 invalid
	 */
	private int validareAnE(String anE) {
		anE = anE.replaceAll("^\\s+", ""); // stergem spatiile din fata
		anE = anE.replaceAll("\\s+$", ""); // stergem spatiile din spate
		try {
			this.anExpirare = Integer.parseInt(anE);
			if (this.anExpirare < this.anFabricatie) {
				return -1;
			} else {
				return 1;
			}
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Valideaza anul fabricatiei
	 * 
	 * @param anF anul introdus
	 * @return 1 valid, -1 invalid
	 */
	private int validareAnF(String anF) {
		anF = anF.replaceAll("^\\s+", ""); // stergem spatiile din fata
		anF = anF.replaceAll("\\s+$", ""); // stergem spatiile din spate
		try {
			this.anFabricatie = Integer.parseInt(anF);
			return 1;
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Valideaza numele producatorului
	 * 
	 * @param producator producatorul introdus
	 * @return 1 valid, -1 invalid
	 */
	private int validareProducator(String producator) {
		String denumire = this.validareDenumire(producator);
		if (denumire == null) {
			return -1;
		} else {
			this.producator = denumire;
			return 1;
		}
	}

	/**
	 * Valideaza un nume
	 * 
	 * @param nume numele introdus
	 * @return 1 pentru valid, -1 daca e invalid
	 */
	private int validareNume(String nume) {
		String denumire = this.validareDenumire(nume);
		if (denumire == null) {
			return -1;
		} else {
			this.nume = denumire;
			return 1;
		}
	}

	/**
	 * Valideaza o denumire
	 * 
	 * @param nume numele introdus
	 * @return returneaza String-ul valid sau null daca nu s-a putut valida
	 */
	private String validareDenumire(String nume) {
		nume = nume.replaceAll("^\\s+", ""); // stergem spatiile din fata
		nume = nume.replaceAll("\\s+$", ""); // stergem spatiile din spate
		nume = nume.replaceAll("\\s+", " "); // stergem spatiile interioare care sunt in plus
		if (nume.length() < 1)
			return null; // verificam daca exista ceva ramas

		for (int i = 0; i < nume.length(); i++) {
			if (!((nume.charAt(i) >= 'a' && nume.charAt(i) <= 'z') || (nume.charAt(i) >= 'A' && nume.charAt(i) <= 'Z')
					|| nume.charAt(i) == ' ')) {
				return null;
			}
		}

		nume = nume.toLowerCase();
		return nume;
	}
}
