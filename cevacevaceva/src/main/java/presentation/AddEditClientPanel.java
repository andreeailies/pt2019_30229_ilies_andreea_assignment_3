package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogicLayer.ValidareClient;
import dataAccessLayer.ClientDAO;
import model.Client;

public class AddEditClientPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	/** Fereastra de baza a panoului*/
	private MenuWindow menu;
	/** Clientul de manipulat */
	private Client client;
	/** Butonul de revenire la meniu*/
	private JButton back = new JButton("Back to Menu");
	/** Butonul de adaugare/editare client*/
	private JButton add = new JButton("Add new Client");

	private JTextField nume = new JTextField(20);
	private JTextField prenume = new JTextField(20);
	private JTextField adresa = new JTextField(20);
	private JTextField telefon = new JTextField(20);
	private JTextField varsta = new JTextField(20);

	/**
	 * Constructor pentru panoul de adaugare/editare client
	 * @param x fereastra 
	 * @param client clientul de editat, daca e null atunci trebuie adaugat
	 */
	public AddEditClientPanel(MenuWindow x, Client client) {
		this.menu = x;
		this.client = client;
		x.setContentPane(this);

		this.setUpPanel();
		
		if (this.client != null) {
			this.scrieInEtichete();
			this.add.setText("Finish edit");
		}
		this.ascultatori();
	}

	/**
	 * Modifica anumite proprietati ale unui label
	 * @param label labelul de modificat
	 * @param color culoarea scrisului
	 * @param size marimea scrisului
	 * @param align 1 = centru, altfel dreapta
	 */
	private void setUpLabel(JLabel label, Color color, int size, int align) {
		label.setForeground(color);
		if (align == 1) {
			label.setAlignmentX(CENTER_ALIGNMENT);
		} else {
			label.setAlignmentX(RIGHT_ALIGNMENT);
		}
		label.setFont(new Font("Times New Roman", Font.PLAIN, size));
	}

	/**
	 * Pregateste vizual panoul
	 */
	private void setUpPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBackground(new Color(66, 134, 244));
		this.add(new JLabel(" "));

		JLabel titlu;
		if (client == null) {
			titlu = new JLabel("Add a New Client");
		} else {
			titlu = new JLabel("Edit Client with ID " + client.getId());
		}
		this.setUpLabel(titlu, Color.black, 20, 1);
		this.add(titlu);
		this.add(new JLabel(" "));

		JLabel numeL = new JLabel("First Name:");
		this.setUpLabel(numeL, Color.white, 15, 1);
		this.add(numeL);
		this.add(nume);

		JLabel prenumeL = new JLabel("Last Name:");
		this.setUpLabel(prenumeL, Color.white, 15, 1);
		this.add(prenumeL);
		this.add(prenume);

		JLabel adresaL = new JLabel("Address:");
		this.setUpLabel(adresaL, Color.white, 15, 1);
		this.add(adresaL);
		this.add(adresa);

		JLabel telefonL = new JLabel("Telephone:");
		this.setUpLabel(telefonL, Color.white, 15, 1);
		this.add(telefonL);
		this.add(telefon);

		JLabel varstaL = new JLabel("Age:");
		this.setUpLabel(varstaL, Color.white, 15, 1);
		this.add(varstaL);
		this.add(varsta);

		JPanel aux = new JPanel();
		aux.setBackground(new Color(66, 134, 244));
		aux.add(add);
		aux.add(new JLabel(" "));
		aux.add(back);
		this.add(aux);

		this.menu.pack();
	}

	private void ascultatori() {

		/**
		 * ascultator pentru butonul de intoarcere la meniu
		 */
		this.back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AddEditClientPanel.this.menu.panouMeniu.setVisible(true);
				AddEditClientPanel.this.menu.setContentPane(AddEditClientPanel.this.menu.panouMeniu);
				AddEditClientPanel.this.menu.remove(AddEditClientPanel.this);
				AddEditClientPanel.this.menu.pack();
			}
		});

		/**
		 * ascultator pentru butonul de adaugare/editare
		 */
		this.add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ValidareClient valid = new ValidareClient();
				int validat = valid.validareClient(AddEditClientPanel.this.getNume(), AddEditClientPanel.this.getPrenume(),
						AddEditClientPanel.this.getAdresa(), AddEditClientPanel.this.getTelefon(),
						AddEditClientPanel.this.getVarsta());
				switch (validat) {
				case 1:
					if (AddEditClientPanel.this.client == null) {
						AddEditClientPanel.this.client = valid.getClientValidat(0);
						ClientDAO x = new ClientDAO();
						x.insert(AddEditClientPanel.this.client);
						JOptionPane.showMessageDialog(null, "Client added!", "Success!",  JOptionPane.INFORMATION_MESSAGE);
					} else {
						AddEditClientPanel.this.client = valid.getClientValidat(AddEditClientPanel.this.client.getId());
						ClientDAO x = new ClientDAO();
						x.update(AddEditClientPanel.this.client, AddEditClientPanel.this.client.getId());
						JOptionPane.showMessageDialog(null, "Client edited!", "Success!",  JOptionPane.INFORMATION_MESSAGE);
					}
					break;
				case -1:
					JOptionPane.showMessageDialog(null, "Last name isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
					break;
				case -2:
					JOptionPane.showMessageDialog(null, "First name isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
					break;
				case -3:
					JOptionPane.showMessageDialog(null, "Address isn't right!", "Fail!",  JOptionPane.ERROR_MESSAGE);
					break;
				case -4:
					JOptionPane.showMessageDialog(null, "Telephone isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
					break;
				default:
					JOptionPane.showMessageDialog(null, "Age isn't right!", "Fail!",  JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
	
	/**
	 * Scrie in panou datele clientului
	 */
	private void scrieInEtichete() {
		this.nume.setText(client.getNume());
		this.prenume.setText(client.getPrenume());
		this.adresa.setText(client.getAdresa());
		this.telefon.setText(client.getTelefon());
		this.varsta.setText(Integer.toString(client.getVarsta()));
	}

	public String getNume() {
		return nume.getText();
	}

	public void setNume(JTextField nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume.getText();
	}

	public void setPrenume(JTextField prenume) {
		this.prenume = prenume;
	}

	public String getAdresa() {
		return adresa.getText();
	}

	public void setAdresa(JTextField adresa) {
		this.adresa = adresa;
	}

	public String getTelefon() {
		return telefon.getText();
	}

	public void setTelefon(JTextField telefon) {
		this.telefon = telefon;
	}

	public String getVarsta() {
		return varsta.getText();
	}

	public void setVarsta(JTextField varsta) {
		this.varsta = varsta;
	}

}
