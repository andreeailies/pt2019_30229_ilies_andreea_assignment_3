package presentation;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

import model.Client;
import model.Comanda;
import model.Produs;

public class Factura {

	private Comanda comanda;
	private Client client;
	private Produs produs;

	/** 
	 * Constructor pentru o factura
	 * @param comanda comanda de facturat
	 * @param client clientul ce a dat comanda
	 * @param produs produsul comandat
	 */
	public Factura(Comanda comanda, Client client, Produs produs) {
		this.comanda = comanda;
		this.client = client;
		this.produs = produs;
		
		FileWriter write = null;
		PrintWriter printer = null;
		try {
			write = new FileWriter("factura" + this.comanda.getId() + ".txt", false);
			printer = new PrintWriter(write);

			printer.println("Factura " + comanda.getId() + " a aplicatiei");
			printer.println(" ");
			this.writeClient(printer);
			this.writeProdus(printer);
			
			printer.println("----------------------------");
			printer.println(produs.getPret() + " x " + comanda.getCantitate() + " = " + comanda.getPret());
			printer.println("data: " + comanda.getData());
			printer.println("----------------------------");
			printer.println("Multumim ca ati cumparat de la noi!");
			
		} catch (Exception eee) {
			JOptionPane.showMessageDialog(null, "Nu am putut scrie in document!", "Failed!", JOptionPane.ERROR_MESSAGE);
		} finally {
			if (printer != null)
				printer.close();
		}
	}

	/**
	 * Scrie datele clientului pe factura
	 * @param printer printerul cu care printam in fisierul text
	 * @throws IOException
	 */
	private void writeClient(PrintWriter printer) throws IOException {
		printer.println("clientul: " + client.getNume() + " " + client.getPrenume());
		printer.println("ID: " + client.getId());
		printer.println("domiciliat la: " + client.getAdresa());
		printer.println(client.getTelefon());
		
	}

	/**
	 * Scrie datele produsului pe factura
	 * @param printer printerul cu care printam in fisierul text
	 * @throws IOException
	 */
	private void writeProdus(PrintWriter printer) throws IOException {
		printer.println("A cumparat produsul cu ID-ul " + produs.getId() + " ("+ produs.getNume() + ")");
		printer.println("producator: " + produs.getProducator());
		printer.println("valabilitate intre: " + produs.getAnFabricatie() + " - " + produs.getAnExpirare());
		
	}
}
