package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;


import dataAccessLayer.ClientDAO;
import dataAccessLayer.CreareTabel;
import dataAccessLayer.ProdusDAO;
import model.Client;
import model.Produs;

public class ViewAllPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private MenuWindow menu;
	private JTable tabel;
	private int obiect;
	private JButton back = new JButton("Back to Menu");

	/**
	 * Constructor pentru un panou ce afiseaza toate datele dintr-un anumit tabel
	 * @param x fereastra
	 * @param obiect 1 daca e pentru Client, altfel pentru Produs
	 */
	public ViewAllPanel(MenuWindow x, int obiect) {
		this.menu = x;
		this.obiect = obiect;
		x.setContentPane(this);

		this.setMaximumSize(new Dimension(400, 500));
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBackground(new Color(66, 134, 244));
		this.add(new JLabel(" "));

		JLabel label;
		if (this.obiect == 1) {
			label = new JLabel("These are our clients:");
		} else {
			label = new JLabel("These are our products:");
		}
		
		label.setForeground(Color.white);
		label.setAlignmentX(CENTER_ALIGNMENT);
		label.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		this.add(label);
		this.add(new JLabel(" "));
		
		if (obiect == 1) {
			ClientDAO dao = new ClientDAO();
			List<Client> data = dao.findAll();
			tabel = CreareTabel.createTable(data);
		} else {
			ProdusDAO dao = new ProdusDAO();
			List<Produs> data = dao.findAll();
			tabel = CreareTabel.createTable(data);
		}

		JScrollPane scroll = new JScrollPane(tabel);
		scroll.setMaximumSize(new Dimension(400, 400));
		scroll.setVisible(true);
		this.add(scroll);
		this.add(new JLabel(" "));
		back.setAlignmentX(CENTER_ALIGNMENT);
		this.add(back);
		this.add(new JLabel(" "));
		this.menu.pack();

		this.ascultatori();
	}

	/**
	 * Ascultator pentru butonul de intoarcere la meniu
	 */
	private void ascultatori() {

		this.back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ViewAllPanel.this.menu.panouMeniu.setVisible(true);
				ViewAllPanel.this.menu.setContentPane(ViewAllPanel.this.menu.panouMeniu);
				ViewAllPanel.this.menu.remove(ViewAllPanel.this);
				ViewAllPanel.this.menu.pack();
			}
		});
	}

}
