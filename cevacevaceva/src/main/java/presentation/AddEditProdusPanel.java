package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogicLayer.ValidareProdus;
import dataAccessLayer.ProdusDAO;
import model.Produs;

public class AddEditProdusPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private MenuWindow menu;
	private Produs produs;

	private JButton back = new JButton("Back to Menu");
	private JButton add = new JButton("Add new Product");

	private JTextField nume = new JTextField(20);
	private JTextField producator = new JTextField(20);
	private JTextField anF = new JTextField(20);
	private JTextField anE = new JTextField(20);
	private JTextField pret = new JTextField(20);
	private JTextField cantitate = new JTextField(20);

	/**
	 * Constructor pentru panelul de adaugare/editare produs
	 * @param x fereastra 
	 * @param produs produsul de editat, daca e null atunci trebuie adaugat
	 */
	public AddEditProdusPanel(MenuWindow x, Produs produs) {
		this.menu = x;
		this.produs = produs;
		x.setContentPane(this);

		this.setUpPanel();

		if (this.produs != null) {
			this.scrieInEtichete();
			this.add.setText("Finish edit");
		}
		this.ascultatori();
	}

	private void setUpLabel(JLabel label, Color color, int size, int align) {
		label.setForeground(color);
		if (align == 1) {
			label.setAlignmentX(CENTER_ALIGNMENT);
		} else {
			label.setAlignmentX(RIGHT_ALIGNMENT);
		}
		label.setFont(new Font("Times New Roman", Font.PLAIN, size));
	}

	private void setUpPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBackground(new Color(66, 134, 244));
		this.add(new JLabel(" "));

		JLabel titlu;
		if (produs == null) {
			titlu = new JLabel("Add a New Product");
		} else {
			titlu = new JLabel("Edit Product with ID " + produs.getId());
		}
		this.setUpLabel(titlu, Color.black, 20, 1);
		this.add(titlu);
		this.add(new JLabel(" "));

		JLabel numeL = new JLabel("Name:");
		this.setUpLabel(numeL, Color.white, 15, 1);
		this.add(numeL);
		this.add(nume);

		JLabel producatorL = new JLabel("Manufacturer:");
		this.setUpLabel(producatorL, Color.white, 15, 1);
		this.add(producatorL);
		this.add(producator);

		JLabel anFL = new JLabel("Made in year:");
		this.setUpLabel(anFL, Color.white, 15, 1);
		this.add(anFL);
		this.add(anF);

		JLabel anEL = new JLabel("Expires in year:");
		this.setUpLabel(anEL, Color.white, 15, 1);
		this.add(anEL);
		this.add(anE);

		JLabel pretL = new JLabel("Price:");
		this.setUpLabel(pretL, Color.white, 15, 1);
		this.add(pretL);
		this.add(pret);

		JLabel cantitateL = new JLabel("Quantity:");
		this.setUpLabel(cantitateL, Color.white, 15, 1);
		this.add(cantitateL);
		this.add(cantitate);

		JPanel aux = new JPanel();
		aux.setBackground(new Color(66, 134, 244));
		aux.add(add);
		aux.add(new JLabel(" "));
		aux.add(back);
		this.add(aux);

		this.menu.pack();
	}

	private void ascultatori() {
		this.back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AddEditProdusPanel.this.menu.panouMeniu.setVisible(true);
				AddEditProdusPanel.this.menu.setContentPane(AddEditProdusPanel.this.menu.panouMeniu);
				AddEditProdusPanel.this.menu.remove(AddEditProdusPanel.this);
				AddEditProdusPanel.this.menu.pack();
			}
		});

		this.add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ValidareProdus valid = new ValidareProdus();
				int validat = valid.validareProdus(AddEditProdusPanel.this.getNume(), AddEditProdusPanel.this.getProducator(),
						AddEditProdusPanel.this.getAnF(), AddEditProdusPanel.this.getAnE(),
						AddEditProdusPanel.this.getPret(), AddEditProdusPanel.this.getCantitate());
				switch (validat) {
				case 1:
					if (AddEditProdusPanel.this.produs == null) {
						AddEditProdusPanel.this.produs = valid.getProdusValidat(0);
						ProdusDAO x = new ProdusDAO();
						x.insert(AddEditProdusPanel.this.produs);
						JOptionPane.showMessageDialog(null, "Product added!", "Success!",  JOptionPane.INFORMATION_MESSAGE);
					} else {
						AddEditProdusPanel.this.produs = valid.getProdusValidat(AddEditProdusPanel.this.produs.getId());
						ProdusDAO x = new ProdusDAO();
						x.update(AddEditProdusPanel.this.produs, AddEditProdusPanel.this.produs.getId());
						JOptionPane.showMessageDialog(null, "Product edited!", "Success!",  JOptionPane.INFORMATION_MESSAGE);
					}
					break;
				case -1:
					JOptionPane.showMessageDialog(null, "Name isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
					break;
				case -2:
					JOptionPane.showMessageDialog(null, "Manufacturer isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
					break;
				case -3:
					JOptionPane.showMessageDialog(null, "Fabrication year isn't right!", "Fail!",  JOptionPane.ERROR_MESSAGE);
					break;
				case -4:
					JOptionPane.showMessageDialog(null, "Expiration isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
					break;
				case -5:
					JOptionPane.showMessageDialog(null, "Price isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
					break;
				default:
					JOptionPane.showMessageDialog(null, "Quantity isn't right!", "Fail!",  JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	private void scrieInEtichete() {
		this.nume.setText(produs.getNume());
		this.producator.setText(produs.getProducator());
		this.anF.setText(Integer.toString(produs.getAnFabricatie()));
		this.anE.setText(Integer.toString(produs.getAnExpirare()));
		this.cantitate.setText(Integer.toString(produs.getCantitate()));
		this.pret.setText(Float.toString(produs.getPret()));
	}

	public String getNume() {
		return nume.getText();
	}

	public void setNume(JTextField nume) {
		this.nume = nume;
	}

	public String getProducator() {
		return producator.getText();
	}

	public void setProducator(JTextField producator) {
		this.producator = producator;
	}

	public String getAnF() {
		return anF.getText();
	}

	public void setAnF(JTextField anF) {
		this.anF = anF;
	}

	public String getAnE() {
		return anE.getText();
	}

	public void setAnE(JTextField anE) {
		this.anE = anE;
	}

	public String getPret() {
		return pret.getText();
	}

	public void setPret(JTextField pret) {
		this.pret = pret;
	}

	public String getCantitate() {
		return cantitate.getText();
	}

	public void setCantitate(JTextField cantitate) {
		this.cantitate = cantitate;
	}

}
