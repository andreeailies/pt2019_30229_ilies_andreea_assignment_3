package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogicLayer.ValidareComanda;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ComandaDAO;
import dataAccessLayer.ProdusDAO;
import model.Client;
import model.Comanda;
import model.Produs;

public class OrderPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private MenuWindow menu;

	private JComboBox<Client> clienti = new JComboBox<>();
	private JComboBox<Produs> produse = new JComboBox<>();

	private JTextField cantitate = new JTextField(20);

	private JButton order = new JButton("Place Order");
	private JButton back = new JButton("Back to the Menu");

	/**
	 * Constructorul panoului de efectuat o comanda noua
	 * 
	 * @param x fereastra
	 */
	public OrderPanel(MenuWindow x) {
		this.menu = x;
		x.setContentPane(this);

		this.setUpPanel();
		this.ascultatori();
	}

	/**
	 * Aranjeaza panoul
	 */
	private void setUpPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setBackground(new Color(66, 134, 244));
		this.add(new JLabel(" "));

		JLabel titlu = new JLabel("Place a New Order");
		this.setUpLabel(titlu, Color.black, 20, 1);
		this.add(titlu);
		this.add(new JLabel(" "));

		this.setUpCombo(clienti, 1);
		this.setUpCombo(produse, 2);

		JLabel clientL = new JLabel("Choose an existing Client:");
		this.setUpLabel(clientL, Color.white, 15, 1);
		this.add(clientL);
		this.add(clienti);

		JLabel produsL = new JLabel("Choose an existing Product:");
		this.setUpLabel(produsL, Color.white, 15, 1);
		this.add(produsL);
		this.add(produse);

		JLabel canti = new JLabel("Choose quantity:");
		this.setUpLabel(canti, Color.white, 15, 1);
		this.add(canti);
		this.add(cantitate);
		this.add(new JLabel(" "));

		JPanel aux = new JPanel();
		aux.setBackground(new Color(66, 134, 244));
		aux.add(order);
		aux.add(new JLabel(" "));
		aux.add(back);
		this.add(aux);

		this.menu.pack();
	}

	/**
	 * Incarca un comboBox cu obiectele din tabelul corespondent
	 * @param combo  comboBox-ul de incarcat cu obiecte
	 * @param obiect 1 daca obiectele sunt Client, altfel Produs
	 */
	@SuppressWarnings("unchecked")
	private <T> void setUpCombo(JComboBox<T> combo, int obiect) {
		if (obiect == 1) {
			ClientDAO x = new ClientDAO();
			List<T> lista = (List<T>) x.findAll();
			combo.setModel(new DefaultComboBoxModel<T>(new Vector<T>(lista)));
		} else {
			ProdusDAO y = new ProdusDAO();
			List<T> lista = (List<T>) y.findAll();
			combo.setModel(new DefaultComboBoxModel<T>(new Vector<T>(lista)));
		}
	}

	private void setUpLabel(JLabel label, Color color, int size, int align) {
		label.setForeground(color);
		if (align == 1) {
			label.setAlignmentX(CENTER_ALIGNMENT);
		} else {
			label.setAlignmentX(RIGHT_ALIGNMENT);
		}
		label.setFont(new Font("Times New Roman", Font.PLAIN, size));
	}

	private void ascultatori() {
		this.back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				OrderPanel.this.menu.panouMeniu.setVisible(true);
				OrderPanel.this.menu.setContentPane(OrderPanel.this.menu.panouMeniu);
				OrderPanel.this.menu.remove(OrderPanel.this);
				OrderPanel.this.menu.pack();
			}
		});

		this.order.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ValidareComanda validare = new ValidareComanda(((Client) clienti.getSelectedItem()).getId(),
						((Produs) produse.getSelectedItem()).getId());
				int validat = validare.validareComanda(cantitate.getText());
				switch (validat) {
				case -1:
					JOptionPane.showMessageDialog(null, "Quantity format isn't right!", "Fail!",
							JOptionPane.ERROR_MESSAGE);
					break;
				case -2:
					JOptionPane.showMessageDialog(null, "We don't have enough of this!", "Fail!",
							JOptionPane.ERROR_MESSAGE);
					break;
				default:
					ComandaDAO x = new ComandaDAO();
					Comanda comanda = validare.getComandaValida();
					x.insert(comanda);
					@SuppressWarnings("unused")
					Factura factura = new Factura(comanda, (Client) clienti.getSelectedItem(),
							(Produs) produse.getSelectedItem());
					((Produs) produse.getSelectedItem())
							.setCantitate(((Produs) produse.getSelectedItem()).getCantitate() - comanda.getCantitate());
					ProdusDAO z = new ProdusDAO();
					z.update((Produs) produse.getSelectedItem(), ((Produs) produse.getSelectedItem()).getId());
					JOptionPane.showMessageDialog(null, "The order is OK!", "Success!",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}

		});
	}

}
