package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import businessLogicLayer.ValidareClient;
import businessLogicLayer.ValidareProdus;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.ProdusDAO;
import model.Client;
import model.Produs;

public class MenuWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	private JButton addClient = new JButton("add");
	private JButton editClient = new JButton("edit");
	private JButton deleteClient = new JButton("delete");
	private JButton viewClient = new JButton("view all");

	private JButton addProdus = new JButton("add");
	private JButton editProdus = new JButton("edit");
	private JButton deleteProdus = new JButton("delete");
	private JButton viewProdus = new JButton("view all");

	private JButton order = new JButton("Place A New Order");

	public JPanel panouMeniu = new JPanel();

	/** Constructor pentru initializarea ferestrei */
	public MenuWindow() {
		this.setMinimumSize(new Dimension(400, 300));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setContentPane(panouMeniu);

		this.aspectButoane();
		this.ascultatoriButoane();
		this.setUpMenu();
	}

	/** Modeleaza aspectul butoanelor */
	private void aspectButoane() {
		this.setUpButton(addClient, Color.white, new Color(239, 179, 110));
		this.setUpButton(editClient, new Color(239, 179, 110), Color.black);
		this.setUpButton(deleteClient, Color.white, new Color(239, 179, 110));
		this.setUpButton(viewClient, new Color(239, 179, 110), Color.black);

		this.setUpButton(addProdus, new Color(239, 179, 110), Color.black);
		this.setUpButton(editProdus, Color.white, new Color(239, 179, 110));
		this.setUpButton(deleteProdus, new Color(239, 179, 110), Color.black);
		this.setUpButton(viewProdus, Color.white, new Color(239, 179, 110));

		this.setUpButton(order, new Color(239, 179, 110), Color.white);

	}

	private void setUpButton(JButton buton, Color colorB, Color colorT) {
		buton.setBackground(colorB);
		buton.setForeground(colorT);
	}

	private void setUpLabel(JLabel label, Color color, int size, int align) {
		label.setForeground(color);
		if (align == 1) {
			label.setAlignmentX(CENTER_ALIGNMENT);
		} else {
			// label.setAlignmentX(LEFT_ALIGNMENT);
			label.setAlignmentX(RIGHT_ALIGNMENT);
		}
		label.setFont(new Font("Times New Roman", Font.PLAIN, size));
	}

	private void setUpMenu() {
		panouMeniu.setLayout(new BoxLayout(panouMeniu, BoxLayout.Y_AXIS));
		panouMeniu.setBackground(new Color(66, 134, 244));
		panouMeniu.add(new JLabel(" "));
		JLabel manageClients = new JLabel("Manage Clients");
		this.setUpLabel(manageClients, Color.white, 20, 1);
		panouMeniu.add(manageClients);

		JPanel aux1 = new JPanel();
		aux1.setBackground(new Color(66, 134, 244));
		aux1.add(addClient);
		aux1.add(new JLabel(" "));
		aux1.add(editClient);
		aux1.add(new JLabel(" "));
		aux1.add(deleteClient);
		aux1.add(new JLabel(" "));
		aux1.add(viewClient);
		panouMeniu.add(aux1);

		JLabel manageProduct = new JLabel("Manage Products");
		this.setUpLabel(manageProduct, Color.black, 20, 1);
		panouMeniu.add(manageProduct);

		JPanel aux2 = new JPanel();
		aux2.setBackground(new Color(66, 134, 244));
		aux2.add(addProdus);
		aux2.add(new JLabel(" "));
		aux2.add(editProdus);
		aux2.add(new JLabel(" "));
		aux2.add(deleteProdus);
		aux2.add(new JLabel(" "));
		aux2.add(viewProdus);
		panouMeniu.add(aux2);

		order.setAlignmentX(CENTER_ALIGNMENT);
		panouMeniu.add(order);
		panouMeniu.add(new JLabel(" "));

		this.pack();
	}

	private void ascultatoriButoane() {
		this.ascultatoriButoaneClient();
		this.ascultatoriButoaneProdus();

		order.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				 @SuppressWarnings("unused")
				OrderPanel orderP = new OrderPanel(MenuWindow.this);
			}
		});
	}

	private void ascultatoriButoaneClient() {

		addClient.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MenuWindow.this.panouMeniu.setVisible(false);
				@SuppressWarnings("unused")
				AddEditClientPanel adC = new AddEditClientPanel(MenuWindow.this, null);
			}
		});

		editClient.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String idS = JOptionPane.showInputDialog(null, "Enter the ID of the client to be edited", "ID input",
						JOptionPane.OK_OPTION);
				if (idS == null)
					return;
				idS = idS.replaceAll("^\\s+", ""); // stergem spatiile din fata
				idS = idS.replaceAll("\\s+$", ""); // stergem spatiile din spate
				try {
					int idCautat = Integer.parseInt(idS);
					ValidareClient validare = new ValidareClient();
					int valid = validare.existaId(idCautat);
					if (valid == 0) return; 
					ClientDAO cc = new ClientDAO();
					Client client = cc.findById(idCautat);
					
					@SuppressWarnings("unused")
					AddEditClientPanel edC = new AddEditClientPanel(MenuWindow.this, client);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ID isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		deleteClient.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String idS = JOptionPane.showInputDialog(null, "Enter the ID of the client to be deleted", "ID input",
						JOptionPane.OK_OPTION);
				if (idS == null)
					return;
				idS = idS.replaceAll("^\\s+", ""); // stergem spatiile din fata
				idS = idS.replaceAll("\\s+$", ""); // stergem spatiile din spate
				try {
					int idCautat = Integer.parseInt(idS);
					ValidareClient validare = new ValidareClient();
					int valid = validare.existaId(idCautat);
					if (valid == 0) return;
					ClientDAO x = new ClientDAO();
					x.delete(idCautat);
					JOptionPane.showMessageDialog(null, "Client deleted!", "Success!", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ID isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		viewClient.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				@SuppressWarnings("unused")
				ViewAllPanel viewC = new ViewAllPanel(MenuWindow.this, 1);
			}
		});

	}

	private void ascultatoriButoaneProdus() {

		addProdus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MenuWindow.this.panouMeniu.setVisible(false);
				@SuppressWarnings("unused")
				AddEditProdusPanel adC = new AddEditProdusPanel(MenuWindow.this, null);
			}
		});

		editProdus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String idS = JOptionPane.showInputDialog(null, "Enter the ID of the product to be edited", "ID input",
						JOptionPane.OK_OPTION);
				if (idS == null)
					return;
				idS = idS.replaceAll("^\\s+", ""); // stergem spatiile din fata
				idS = idS.replaceAll("\\s+$", ""); // stergem spatiile din spate
				try {
					int idCautat = Integer.parseInt(idS);
					ValidareProdus validare = new ValidareProdus();
					int valid = validare.existaId(idCautat);
					if (valid == 0) return; 
					ProdusDAO cc = new ProdusDAO();
					Produs produs = cc.findById(idCautat);

					@SuppressWarnings("unused")
					AddEditProdusPanel edC = new AddEditProdusPanel(MenuWindow.this, produs);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ID isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		deleteProdus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String idS = JOptionPane.showInputDialog(null, "Enter the ID of the product to be deleted", "ID input",
						JOptionPane.OK_OPTION);
				if (idS == null)
					return;
				idS = idS.replaceAll("^\\s+", ""); // stergem spatiile din fata
				idS = idS.replaceAll("\\s+$", ""); // stergem spatiile din spate
				try {
					int idCautat = Integer.parseInt(idS);
					ValidareProdus validare = new ValidareProdus();
					int valid = validare.existaId(idCautat);
					if (valid == 0) return;
					ProdusDAO x = new ProdusDAO();
					x.delete(idCautat); System.out.println(idCautat);

					JOptionPane.showMessageDialog(null, "Product deleted!", "Success!",
							JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ID isn't right!", "Fail!", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		viewProdus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				@SuppressWarnings("unused")
				ViewAllPanel viewP = new ViewAllPanel(MenuWindow.this, 2);
			}
		});

	}
}
