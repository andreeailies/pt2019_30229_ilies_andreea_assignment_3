package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbstractDAO<T> {

	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}

	/**
	 * Creeaza comanda de executat sub forma de String
	 * @param field numele field-ului dupa care filtram
	 * @param all true daca vrem sa se afiseze tot, false pentru cautare dupa field
	 * @return returneaza comanda creata
	 */
	private String createSelectQuery(String field, boolean all) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		if (all == false) {
		sb.append(" WHERE " + field + " =?");
		}
		return sb.toString();
	}

	/**
	 * Listeaza toate randurile si campurile posibile ale tabelului
	 * @return o lista continand toate randurile/obiectele din tabela
	 */
	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id", true);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			//statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
	/**
	 * Verifica care este id-ul maxim curent
	 * @return id-ul maxim din tabel
	 */
	public int idMax() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id", true);
		query = query.replace("*", "max(id)");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			resultSet.next();
			int max = resultSet.getInt(1); 
			return max;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return 0;
	}

	/**
	 * Cautam un rand dupa id
	 * @param id id-ul cautat
	 * @return randul/obiectul cautat sau null daca nu am gasit nimic
	 */
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id", false);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			List<T> gasit = createObjects(resultSet);
			if (gasit.isEmpty()) return null;
			else return gasit.get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}

	/**
	 * Transforma rezultatul interogarii intr-o lista
	 * @param resultSet rezultatul interogarii
	 * @returno lista de obiecte reprezentand rezultatul interogarii
	 */
	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				@SuppressWarnings("deprecation")
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Creeaza comanda e inserare
	 * @param obiect obiect pentru a scoate field-urile din el
	 * @return comanda
	 */
	private String createInsertQuery(Object obiect) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName());
		sb.append(" VALUES (");
		Vector<Object> header = CreareTabel.retrieveProperties(obiect);
		for(Object x: header) {
			if (x instanceof String == true) sb.append("'"+ x + "'");
			else sb.append(x);
			sb.append(", ");
		}
		sb.delete(sb.length()-2, sb.length()-1);
		sb.append(")");
		return sb.toString();
	}

	/**
	 * Insereaza in baza de date un obiect de tipul T
	 * @param t obiectul de inserat in baza de date
	 * @return obiectul de inserat sau null daca nu reuseste
	 */
	public T insert(T t) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createInsertQuery(t);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return t;
	}
	
	/**
	 * Creeaza comanda pentru update
	 * @param obiect obiectul din care extragem field-urile necesare
	 * @return un String reprezentand comanda 
	 */
	private String createUpdateQuery(Object obiect) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName());
		sb.append("\n SET ");
		Vector<Object> prop = CreareTabel.retrieveProperties(obiect);
		Vector<String> header = CreareTabel.retrieveHeader(obiect); 
		for(int i = 0; i < prop.size(); i++) {
			sb.append(header.elementAt(i) + " = ");
			if (prop.elementAt(i) instanceof String == true) sb.append("'"+ prop.elementAt(i) + "'");
			else sb.append(prop.elementAt(i));
			sb.append(", ");
		}
		
		sb.delete(sb.length()-2, sb.length()-1);
		sb.append("WHERE id = ?");
		return sb.toString();
	}

	/**
	 * Face update unui elemente bazat pe id-ul sau
	 * @param t obiectul cu valorile noi
	 * @param id id-ul obiectului de modificat
	 * @return returneaza obiectul modificat
	 */
	public T update(T t, int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createUpdateQuery(t);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return t;
	}
	/**
	 * Creeaza comanda pentru stergere
	 * @param field campul dupa care filtram informatia
	 * @return comanda pentru stergere
	 */
	private String createDeleteQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}
	
	/**
	 * Sterge un obiect din tabela in functie de id
	 * @param id id-ul dupa care cautam obiectul
	 */
	public void delete(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery("id"); System.out.println(query);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
	}
}
