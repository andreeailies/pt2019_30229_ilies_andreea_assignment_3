package dataAccessLayer;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class CreareTabel {
	
	/**
	 * Creeaza un JTable dintr-o lista de obiecte primita
	 * @param objects lista de obiecte primita
	 * @param <T> generic 
	 * @return returneaza un tabel care contine intreaga lista primita 
	 * si header-ul corepsunzator
	 */
	public static <T> JTable createTable(List<T> objects) {
		JTable tabel = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		if (objects.isEmpty()) return null;
		Vector<String> header = CreareTabel.retrieveHeader(objects.get(0));
		
		for (Object obiect : objects) {
			data.add(CreareTabel.retrieveProperties(obiect));
		}

		DefaultTableModel model =  new DefaultTableModel(data, header);
		tabel = new JTable(model);
		return tabel;
	}
	
	/**
	 * Creeaza un vector ce contine valorile unui obiect 
	 * @param object obiectul a carui valori le extragem
	 * @return un vector cu valorile pentru fiecare camp al obiectului
	 */
	public static Vector<Object> retrieveProperties(Object object) {
		Vector<Object> linie = new Vector<>();
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); 
			Object value;
			try {
				value = field.get(object);
				linie.add(value);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return null;
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				return null;
			}

		}
		return linie;
	}
	
	/**
	 * Extrage numele tuturor campurilor unui obiect de clasa necunoscuta 
	 * @param object obiectul a carui campuri le extragem
	 * @return un vector continand numele fiecarui camp in ordine
	 */
	public static Vector<String> retrieveHeader(Object object) {
		Vector<String> header = new Vector<>();
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); 
			try {
				header.add(field.getName());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return null;
			} 
		}
		return header;
	}
}
